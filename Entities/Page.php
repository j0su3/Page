<?php

namespace Modules\Page\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Tag\Contracts\TaggableInterface;
use Modules\Tag\Traits\TaggableTrait;
use Modules\User\Entities\Sentinel\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Page extends Model implements TaggableInterface
{
    use Translatable, TaggableTrait, NamespacedEntity, MediaRelation;

    protected $table = 'page__pages';
    public $translatedAttributes = [
        'page_id',
        'title',
        'slug',
        /* GOBALO MOD */
        'is_active',
        /* FIN FOBALO MOD */
        'body',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
    ];
    protected $fillable = [
        'is_home',
        'template',
        /* GOBALO MOD */
        'parent_id',
        'status',
        'visibility',
        'author_id',
        'publish_at',
        /* FIN GOBALO MOD */
        // Translatable fields
        'page_id',
        'title',
        'slug',
        /* GOBALO MOD */
        'is_active',
        /* FIN GOBALO MOD */
        'body',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
    ];
    protected $casts = [
        'is_home' => 'boolean'
    ];
    protected $statuses = [
        'published',
        'scheduled',
        'draft'
    ];
    protected $visibilities = [
        'public',
        'private'
    ];
    protected static $entityNamespace = 'asgardcms/page';
    
    /* GOBALO MOD */
    
    public function parent()
    {
        return $this->belongsTo(Page::class,'parent_id');
    }
    
    public function children()
    {
        return $this->hasMany(Page::class,'parent_id')->with('children');
    }
    
    /* FIN GOBALO MOD */
    
    public function author()
    {
        return $this->belongsTo(User::class);
    }
    
    /* GOBALO MOD */
    public function getCanonicalUrl() : string
    {
        if ($this->is_home === true) {         
            return route('page', "/" . $this->defaultLocale);
        }
        
        $page = $this;
        
        $slug = '/' . $page->slug;
        
        while($page->parent_id !== 0)
        {
            $page = Page::find($page->parent_id);
            $slug = '/' . $page->slug . $slug;
        }
        
        $slug = substr($slug, 1);
            
        return route('page', $slug);
    }
    /* FIN GOBALO MOD */

    public function getEditUrl() : string
    {
        return route('admin.page.page.edit', $this->id);
    }

    public function __call($method, $parameters)
    {
        #i: Convert array to dot notation
        $config = implode('.', ['asgard.page.config.relations', $method]);

        #i: Relation method resolver
        if (config()->has($config)) {
            $function = config()->get($config);

            return $function($this);
        }

        #i: No relation found, return the call to parent (Eloquent) to handle it.
        return parent::__call($method, $parameters);
    }
    
    public function getImageAttribute()
    {
        $thumbnail = $this->files()->where('zone', 'image')->first();

        if ($thumbnail === null) {
            return '';
        }

        return $thumbnail;
    }
    
    /* GOBALO MOD */
    public function getPagesHierarchy($id = -1) {
        $childrens = [];
        $children = 'children';
        
        for ($i = 0; $i < 50; $i++) {
            $childrens[$children] = function ($query) use ($id) { $query->where('id', '<>', $id);  $query->where('status', '<>', 'trash'); };
            $children .= '.children';
        }
        
        return Page::with($childrens)->where('parent_id', 0)->where('id', '<>', $id)->where('status', '<>', 'trash')->get()->toArray();
    }
    /* FIN GOBALO MOD */
}
