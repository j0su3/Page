<?php

namespace Modules\Page\Transformers;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class PageTransformer extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'is_home' => $this->is_home,
            'template' => $this->template,
            /* GOBALO MOD */
            'status' => $this->status,
            'visibility' => $this->visibility,
            'author' => $this->author->email,
            'publish_at' => Carbon::parse($this->publish_at)->format('d-m-Y'),
            /* FIN GOBALO MOD */
            'created_at' => $this->created_at->format('d-m-Y'),
            'translations' => [
                'title' => optional($this->translate(locale()))->title,
                'slug' => optional($this->translate(locale()))->slug,
                'is_active' => optional($this->translate(locale()))->is_active
            ],
            'urls' => [
                /* GOBALO MOD */
                'public_url' => $this->getCanonicalUrl(),
                'delete_url_only_trash' => route('api.page.page.destroyonlytrash', $this->id),
                /* FIN GOBALO MOD */
                'delete_url' => route('api.page.page.destroy', $this->id),
                'check_children' => route('api.page.page.checkchildren', $this->id),
            ],
        ];
    }
}
