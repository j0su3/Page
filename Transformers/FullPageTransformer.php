<?php

namespace Modules\Page\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class FullPageTransformer extends Resource
{
    public function toArray($request)
    {
        $pageData = [
            'id' => $this->id,
            'template' => $this->template,
            'is_home' => $this->is_home,
            /* GOBALO MOD */
            'parent_id' => $this->parent_id,
            'parent' => $this->formatTranslations($this->parent),
            'status' => $this->status,
            'visibility' => $this->visibility,
            'publish_at' => $this->publish_at,
            'pages' => $this->formatManyTranslations($this->getPagesHierarchy($this->id)),
            /* FIN GOBALO MOD */
            'urls' => [
                'public_url' => $this->getCanonicalUrl(),
                'check_children' => route('api.page.page.checkchildren', $this->id),
            ],
        ];

        $pageData = $this->setTranslations($pageData);

        /*
        foreach ($this->tags as $tag) {
            $pageData['tags'][] = $tag->name;
        }*/

        return $pageData;
    }

    private function setTranslations($object)
    {
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $object[$locale] = [];
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $object[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
                $object[$locale]['disabled'] = false;

                if( $translatedAttribute === 'is_active' )
                    if( is_null( $object[$locale][$translatedAttribute] ) )
                    {
                        $object[$locale][$translatedAttribute] = false;
                        continue;
                    }

            }
            // Check for children
            $object[$locale]['disabled'] = false;
            if( $this->children )
            {
                foreach($this->formatManyTranslations($this->children) as $child)
                {
                    if( $child[$locale]['is_active'] )
                    {
                        $object[$locale]['disabled'] = true;
                        break;
                    }
                }
            }
        }

        return $object;
    }

    static public function formatTranslations($object)
    {
        if( isset( $object['translations'] ) ) {

            foreach ($object['translations'] as $translation) {
                $locale = $translation['locale'];
                $object[$locale] = $translation;
                unset($object[$locale]['locale']);
            }

            unset($object['translations']);

        }

        return $object;
    }

    static public function formatManyTranslations($objects)
    {
        for($i=0;$i < count($objects);$i++)
        {
            if( isset($objects[$i]['children']) )
                if( $objects[$i]['children'] )
                    $objects[$i]['children'] = FullPageTransformer::formatManyTranslations($objects[$i]['children']);

            $objects[$i] = FullPageTransformer::formatTranslations($objects[$i]);
        }

        return $objects;
    }
}
