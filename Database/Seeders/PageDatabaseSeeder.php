<?php

namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Page\Repositories\PageRepository;

class PageDatabaseSeeder extends Seeder
{
    /**
     * @var PageRepository
     */
    private $page;

    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    public function run()
    {
        Model::unguard();

        $data = [
            'template' => 'home',
            'is_home' => 1,
            /* GOBALO MOD */
            'status' => 'published',
            'visibility' => 'public',
            'author_id' => 1,
            'es' => [
                'title' => 'Página de inicio',
                'slug' => 'inicio',
                'is_active' => 1,
                'body' => '<p><strong>¡Lo hiciste!</strong></p>
<p>Has instalado GóbaloCMS y estás listo para pasar al <a href="/backend">área de administración</a>.</p>',
                'meta_title' => 'Página de inicio',
            ],
            'en' => [
                'title' => 'Home page',
                'slug' => 'home',
                'is_active' => 1,
                'body' => '<p><strong>You made it!</strong></p>
<p>You\'ve installed GóbaloCMS and are ready to proceed to the <a href="/en/backend">administration area</a>.</p>',
                'meta_title' => 'Home page',
            ],
            /* FIN GOBALO MOD */
        ];
        $this->page->create($data);
    }
}
