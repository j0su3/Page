<?php

use Illuminate\Routing\Router;


/** @var Router $router */
$router->group(['prefix' => '/page', 'middleware' => ['api.token', 'auth.admin']], function (Router $router) {
    $router->get('pages', [
        'as' => 'api.page.page.index',
        'uses' => 'PageController@index',
        'middleware' => 'token-can:page.pages.index',
    ]);
    $router->get('pages-server-side', [
        'as' => 'api.page.page.indexServerSide',
        'uses' => 'PageController@indexServerSide',
        'middleware' => 'token-can:page.pages.index',
    ]);
    $router->get('mark-pages-status', [
        'as' => 'api.page.page.mark-status',
        'uses' => 'UpdatePageStatusController',
        'middleware' => 'token-can:page.pages.edit',
    ]);
    /* GOBALO MOD */
    $router->post('pages/destroyall', [
        'as' => 'api.page.page.destroyall',
        'uses' => 'PageController@destroyAll',
        'middleware' => 'token-can:page.pages.destroy',
    ]);
    $router->post('pages/destroyallonlytrash', [
        'as' => 'api.page.page.destroyallonlytrash',
        'uses' => 'PageController@destroyAllOnlyTrash',
        'middleware' => 'token-can:page.pages.destroy',
    ]);
    $router->delete('pages/{page}/trash', [
        'as' => 'api.page.page.destroyonlytrash',
        'uses' => 'PageController@destroyOnlyTrash',
        'middleware' => 'token-can:page.pages.destroy',
    ]);
    /* FIN GOBALO MOD */
    $router->delete('pages/{page}', [
        'as' => 'api.page.page.destroy',
        'uses' => 'PageController@destroy',
        'middleware' => 'token-can:page.pages.destroy',
    ]);
    /* GOBALO MOD */
    $router->delete('pages/{page}/check-children', [
        'as' => 'api.page.page.checkchildren',
        'uses' => 'PageController@checkChildren',
        'middleware' => 'token-can:page.pages.destroy',
    ]);
    /* FIN GOBALO MOD */
    $router->post('pages', [
        'as' => 'api.page.page.store',
        'uses' => 'PageController@store',
        'middleware' => 'token-can:page.pages.create',
    ]);
    /* GOBALO MOD */
    $router->post('pages/hierarchy', [
        'as' => 'api.page.page.hierarchy',
        'uses' => 'PageController@getPageHierarchy',
        'middleware' => 'token-can:page.pages.edit',
    ]);
    /* FIN GOBALO MOD */
    $router->post('pages/{page}', [
        'as' => 'api.page.page.find',
        'uses' => 'PageController@find',
        'middleware' => 'token-can:page.pages.edit',
    ]);
    $router->post('pages/{page}/edit', [
        'as' => 'api.page.page.update',
        'uses' => 'PageController@update',
        'middleware' => 'token-can:page.pages.edit',
    ]);
    /* GOBALO MOD */
    $router->post('pages/{id}/clone', [
        'as' => 'api.page.page.clone',
        'uses' => 'PageController@clone',
        'middleware' => 'can:page.pages.edit',
    ]);
    /* FIN GOBALO MOD */
    $router->get('templates', 'PageTemplatesController')->name('api.page.page-templates.index');
});
