<?php

namespace Modules\Page\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\CategoryTranslation;
use Modules\Blog\Entities\PostTranslation;
use Modules\Page\Entities\Page;
use Modules\Page\Http\Requests\CreatePageRequest;
use Modules\Page\Http\Requests\UpdatePageRequest;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Transformers\FullPageTransformer;
use Modules\Page\Transformers\PageTransformer;
use Illuminate\Validation\ValidationException;
use Modules\Page\Entities\PageTranslation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    /**
     * @var PageRepository
     */
    private $page;
    /* GOBALO MOD */
    private $defaultLocale;
    private $slugsDB;
    /* FIN GOBALO MOD*/

    public function __construct(PageRepository $page)
    {
        $this->page = $page;
        /* GOBALO MOD */
        $this->defaultLocale = env('LOCALE');
        /* FIN GOBALO MOD */
    }
    
    public function index()
    {
        return PageTransformer::collection($this->page->all());
    }

    public function indexServerSide(Request $request)
    {
        return PageTransformer::collection($this->page->serverPaginationFilteringFor($request));
    }

    public function store(CreatePageRequest $request)
    {
        /* GOBALO MOD */
        // Set is_active default locale always true
        $request->merge($this->modifiyRequest($request));
        
        $this->validateSlug($request);
        $this->isHomepage($request);
        $request->merge($this->setAuthor($request));
        /* FIN GOBALO MOD */
        
        $this->page->create($request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('page::messages.page created'),
        ]);
    }
    
    public function find(Page $page)
    {
        return new FullPageTransformer($page);
    }

    public function update(Page $page, UpdatePageRequest $request)
    {
        /* GOBALO MOD */
        // Set is_active default locale always true
        $request->merge($this->modifiyRequest($request));
        
        $this->validateSlug($request, true);
        $this->isHomepage($request);
        $request->merge($this->setAuthor($request));
        /* FIN GOBALO MOD */
        $this->page->update($page, $request->all());
        
        return response()->json([
            'errors' => false,
            'message' => trans('page::messages.page updated'),
        ]);
    }
    
    /* GOBALO MOD */
    
    /**
     * Slug validation ( Request + Database )
     * @param unknown $request
     * @param boolean $update
     */
    private function validateSlug($request, $update = false)
    {
        $this->placeSlugsDB();
        
        $slugs = [];
        $messages = [];
        
        // Save request slugs
        foreach ($request->requiredLocales() as $key => $value) {
            if( isset($request[$key]['is_active']) )
            {
                // If parent is_active = false this.page.is_active = false
                if( $request['parent'] )
                    if( !$request['parent'][$key]['is_active'] ) {
                        $inputs = $request->all();
                        $inputs[$key]['is_active'] = false;
                        $request->merge($inputs);
                    }
                //
                if ($request[$key]['is_active'])
                    if (isset($request[$key]['slug']))
                        if ($request[$key]['slug'])
                            $slugs[$key] = $request[$key]['slug'];
            }
        }
        
        // Validate each slug against every one else ( except itself )
        foreach ($request->requiredLocales() as $key => $value) {
            if( isset($request[$key]['is_active']) )
                    if( $request[$key]['is_active'] )
                        if( isset($request[$key]['slug']) )
                            if( $request[$key]['slug'] )
                                $messages = $this->addMessages($request, $slugs, $key, $messages, $update);
        }
        
        if( $messages )
        {
            throw ValidationException::withMessages($messages);
        }
    }
    
    private function addMessages($request, $slugs, $key, $messages, $update = false)
    {
        $slugsTemp = $slugs ;
        $slugsDB = $this->slugsDB;
        
        // Remove itself from array
        unset($slugsTemp[$key]);
        
        // Remove current page slugs for validation in update
        if($update) $slugsDB = array_diff($slugsDB, PageTranslation::where('page_id', $request->id)->where('is_active', true)->get()->pluck('slug')->toArray());
        
        // Validate slugs
        if(in_array($request[$key]['slug'], array_merge($slugsTemp, $slugsDB))){
            $messages[$key . '.slug'] = $request->translationMessages()['slug.unique'];
        }
        
        return $messages;
    }
    
    /**
     * Fix is_active null from request
     * @param unknown $inputs
     * @param unknown $locales
     */
    private function fixIsActive($inputs, $locales)
    {
        foreach ($locales as $key => $value){
            // Always true for default language
            if($key === $this->defaultLocale)
            {
                $inputs[$this->defaultLocale]['is_active'] = true;
                continue;
            }
            
            if(isset($inputs[$key]['is_active']))
            {
                if($inputs[$key]['is_active'] ===  null) $inputs[$key]['is_active'] = false;
            }
            else
            {
                $inputs[$key]['is_active'] = false;
            }
        }
        
        return $inputs;
    }
    
    /*
     * Set status published if status is "scheduled" and publish_date is less than now
     */
    private function verifyPublished($request)
    {
        if(!isset($request['publish_at'])) $request['publish_at'] = Carbon::now();
        switch ($request['status'])
        {
            case "published":
                if(strtotime($request['publish_at']) > strtotime($request['current_time']))
                    $request['status'] = 'scheduled';
                    break;
            case "scheduled":
                if(strtotime($request['publish_at']) <= strtotime($request['current_time']))
                    $request['status'] = 'published';
                    break;
        }
        
        return $request;
    }
    
    private function setAuthor($request)
    {
        $inputs = $request->all();
        $inputs['author_id'] = Auth::id();
        
        return $inputs;
    }
    
    /**
     * Set is_active for defautl locale always true and convert is_active from null to false
     * @param unknown $request
     * @return boolean
     */
    private function modifiyRequest($request)
    {
        $inputs = $request->all();
        
        $inputs = $this->fixIsActive($inputs, $request->requiredLocales());
        $inputs = $this->verifyPublished($inputs);
        $this->verifyTrash($request);
        
        return $inputs;
    }

    /**
     * Place all the slugs actives in $slugsDB variable
     */
    private function placeSlugsDB()
    {
        $this->slugsDB = [];
        foreach (array_unique(PageTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }

        foreach (array_unique(CategoryTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }


        foreach (array_unique(PostTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }
    }
    
    /**
     * If is_home is true, set other homepages to false
     * @param unknown $request
     */
    private function isHomepage($request)
    {
        if( $request->is_home ) Page::where('id', '<>', $request->id)->update(['is_home' => 0]);
    }

    public function destroy(Page $page)
    {
        $this->page->destroy($page);
        
        return response()->json([
            'errors' => false,
            'message' => trans('page::messages.page deleted'),
        ]);
    }
    
    public function destroyOnlyTrash(Page $page)
    {
        Page::where('parent_id', $page->id)->update(['parent_id' => 0]);
        
        Page::where('id', $page->id)->update(['status' => 'trash', 'parent_id' => 0, 'is_home' => 0]);
        
        return response()->json([
            'errors' => false,
            'message' => trans('page::messages.page deleted trash'),
        ]);
    }
    
    public function checkChildren(Page $page)
    {
         $data = [];
         $message = "";
         
         if($page->parent_id)
         {
             $pageTitle = ( $page->parent->is_active ? $page->parent->title  : $page->parent->id . ' - ' . trans('translation::translations.Inactive translation')  );
             $message .= trans('page::pages.parent page assigned') . ': <br> <b>' . $pageTitle .'</b>. <br><br>';
         }
         
         if($page->children->count())
         {
             $pages = $page->children;
             $data = $page->children->pluck('title')->toArray();
             $message .= trans('page::pages.children pages assigned') . ': <br>';
             $message .= "<ul>";
             foreach ($pages as $pageAux)
             {
                 $pageTitle = ( $pageAux->is_active ? $pageAux->title : $pageAux->id . ' - ' . trans('translation::translations.Inactive translation')  );
                 $message .= "<li><b>". $pageTitle ."</b></li>";
             }
             $message .= "</ul><br>";
         }
         
         if( $page->parent_id || $page->children->count())
         {
             $message .= trans('core::core.modal.confirmation-message-trash');
         }
         
         return response()->json([
             'errors' => false,
             'message' => $message,
             'data' => $data
         ]);
    }
    
    public function destroyAll(Request $request)
    {
        Page::destroy($request->get('ids'));
        
        return response()->json([
            'errors' => false,
            'message' => trans('core::core.Items deleted successfully'),
        ]);
    }
    
    public function destroyAllOnlyTrash(Request $request)
    {
        // Page::destroy($request->get('ids'));
        
        // Remove parent_id
        $query = Page::query();
        $query2 = Page::query();
        
        foreach ($request->get('ids') as $id) {
            $query->orWhere('parent_id', $id);
            $query2->orWhere('id', $id);
        }
        
        $query->update(['parent_id' => 0]);
        $query2->update(['status' => 'trash', 'is_home' => 0]);
        
        return response()->json([
            'errors' => false,
            'message' => trans('core::core.Items deleted successfully trash'),
        ]);
    }
    
    /**
     * Clone a page
     * @param unknown $id
     */
    public function clone($id)
    {
        $this->placeSlugsDB();

        $page = $this->page->find($id)->toArray();

        // null variables
        $page['id'] = null;
        $page['page_id'] = null;
        $page['parent_id'] = 0;
        $page['is_home'] = false;
        $page['status'] = 'draft';
        $page['visibility'] = 'private';
        $page['author_id'] = Auth::id();
        $page['publish_at'] = Carbon::now();
        
        // Add translations
        foreach($page['translations'] as $translation)
        {
            // Slug unique
            while(in_array($translation['slug'], $this->slugsDB))
            {
                $translation['slug'] .= '-' . trans('translation::translations.copy');
            }
            $page[$translation['locale']] = $translation;
        }
        
        $this->page->create($page);
    }
    
    public function getPageHierarchy()
    {
        return FullPageTransformer::formatManyTranslations((new Page())->getPagesHierarchy());
    }
    
    public function verifyTrash($request)
    {
        if($request->status === 'trash')
        {
            Page::where('parent_id', $request->id)->update(['parent_id' => 0]);
            Page::where('id', $request->id)->update(['parent_id' => 0]);
        }
    }
    /* FIN GOBALO MOD */
}
