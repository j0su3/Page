<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Illuminate\Support\Facades\Auth;

class PublicController extends BasePublicController
{
    /**
     * @var PageRepository
     */
    private $page;
    /**
     * @var Application
     */
    private $app;
    /* GOBALO MOD */
    private $published = 'published';
    private $private = 'private';
    /* FIN GOBALO MOD */
    public function __construct(PageRepository $page, Application $app)
    {
        parent::__construct();
        $this->page = $page;
        $this->app = $app;
    }

    /* GOBALO MOD */
    /**
     * Verify if user ( or no logged user ) can view the page
     * @param unknown $page
     */
    private function viewPagePermission($page)
    {
        // If page available
        if($page)
        {
            // If user logged
            if($this->auth->user())
            {
                // If user is admin there is no exception
                if($this->auth->user()->hasRoleName('admin'))
                {
                    // Admin can view all pages [ Except inactives translates :) Slug unique ]
                }
                else
                {
                    $this->throw404IfNotFound($page);
                }
            }
            else
            {
                $this->throw404IfNotFound($page);
            }
        }
        else
        {
            $this->throw404IfNotFound($page);
        }
            
    }
    
    /* FIN GOBALO MOD */
    
    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug)
    {
        $page = $this->findPageForSlug($slug);
        
        /* GOBALO MOD */
        $this->viewPagePermission($page);
        /* FIN GOBALO MOD */
        
        $currentTranslatedPage = $page->getTranslation(locale());
        
        /*if ($slug !== $currentTranslatedPage->slug) {
            return redirect()->to($currentTranslatedPage->locale . '/' . $currentTranslatedPage->slug, 301);
        }*/
        $template = $this->getTemplateForPage($page);
        
        $alternate = $this->getAlternateMetaData($page);
        
        return view($template, compact('page', 'alternate'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage()
    {
        $page = $this->page->findHomepage();
        
        /* GOBALO MOD */
        $this->viewPagePermission($page);
        /* FIN GOBALO MOD */

        $template = $this->getTemplateForPage($page);

        $alternate = $this->getAlternateMetaData($page);

        return view($template, compact('page', 'alternate'));
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug)
    {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());
        
        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }
        
        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page)
    {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found or draft or ianctive translation
     * @param $page
     */
    private function throw404IfNotFound($page)
    {
        if($this->auth->user()){
            if (null === $page 
                /* GOBALO MOD */
                || $page->status !== $this->published || $page->is_active === false 
                /* FIN GOBALO MOD */
                ) {
                $this->app->abort('404');
            }
        }
        else {
            if (null === $page
                /* GOBALO MOD */
                || $page->status !== $this->published || $page->is_active === false || $page->visibility === $this->private
                /* FIN GOBALO MOD */
                ) {
                    $this->app->abort('404');
                }
        }
    }   

    /**
     * Create a key=>value array for alternate links
     *
     * @param $page
     *
     * @return array
     */
    private function getAlternateMetaData($page)
    {
        $translations = $page->getTranslationsArray();

        $alternate = [];
        foreach ($translations as $locale => $data) {
            $alternate[$locale] = $data['slug'];
        }

        return $alternate;
    }
}
