<?php

namespace Modules\Page\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Core\Internationalisation\BaseFormRequest;

class CreatePageRequest extends BaseFormRequest
{
    protected $translationsAttributesKey = 'page::pages.validation.attributes';

    public function rules()
    {
        /* GOBALO MOD */
        $rules = [];
        $defaultLocale = env('LOCALE');

        foreach ($this->requiredLocales() as $localeKey => $locale) {
            if( $localeKey === $defaultLocale )
            {
                $rules = $this->addRules($rules, $localeKey, true);
            }
            else if( isset($this->request->get($localeKey)['is_active']) )
            {
                if( $this->request->get($localeKey)['is_active'] )
                {
                    $rules = $this->addRules($rules, $localeKey);
                }
            }
        }
        
        return $rules;
        /* FIN GOBALO MOD */
    }

    public function translationRules()
    {
        /* GOBALO MOD */
        return [];
        /* FIN GOBALO MOD */
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            /* GOBALO MOD */
            'title.required' => trans('page::messages.title is required'),
            /* FIN GOBALO MOD */
            'is_home.unique' => trans('page::messages.only one homepage allowed'),
        ];
    }

    public function translationMessages()
    {
        return [
            'title.required' => trans('page::messages.title is required'),
            'slug.required' => trans('page::messages.slug is required'),
            /* GOBALO MOD */
            'slug.regex' => trans('page::messages.slug regex'),
            'slug.unique' => trans('page::messages.slug is unique'),
            'slug.not_in' => trans('page::messages.slug is reserved'),
            /* FIN GOBALO MOD */
        ];
    }
    
    /* GOBALO MOD */
    public function addRules($rules, $localeKey, $defaultLocale = false)
    {
        $rules[$localeKey . '.title'] = 'required';
        $rules[$localeKey . '.slug'] = [ 'required', 'regex:/^[a-zA-Z0-9-_]+$/u' ];
        if( $defaultLocale ) array_push($rules[$localeKey . '.slug'] ,Rule::notIn(array_keys(config('asgard.core.available-locales'))));
        
        return $rules;
    }
    /* FIN GOBALO MOD */
}
